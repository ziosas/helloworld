//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Salvatore Zicaro on 14/07/13.
//  Copyright (c) 2013 Salvatore Zicaro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
