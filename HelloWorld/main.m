//
//  main.m
//  HelloWorld
//
//  Created by Salvatore Zicaro on 14/07/13.
//  Copyright (c) 2013 Salvatore Zicaro. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
